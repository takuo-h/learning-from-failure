 #!/usr/bin/env python
# coding: utf-8

# ------------------------------------------------------
from os import path
import query_queue_and_parallel as qqp
import numpy as np
def main():
	query = qqp.Query()

	query['scripts']		= [path.join(path.dirname(__file__),'code/train.py')]
	query['dataset']		= ['mnist','cifar10']
	query['seed']			= [_ for _ in range(5)]
	query['num_epochs']		= [100]
	query['lr']				= [0.01, 0.05, 0.1, 0.5]
	query.ready()
	query.run(GPU_index=[7])	

if __name__ == '__main__':
	main()



# ----------------------
