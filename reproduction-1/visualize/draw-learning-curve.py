#!/usr/bin/env python
# coding: utf-8

import sys


# -------------------------------------------------------------------
import datetime
def report(*args):
	print(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')+' '+' '.join(map(str,args)).replace('\n',''))


# -------------------------------------------------------------------
# -------------------------------------------------------------------
import os
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
def draw(trails, file_name):
	print(f'draw:{file_name}')
	fig = plt.figure(figsize=(7,3))
	plt.subplots_adjust(left=0.07, right=0.95, top=0.92, bottom=0.15)

	for k,metric in enumerate(['loss','accuracy']):
		ax = fig.add_subplot(1, 2, k+1)
		for i,data in enumerate(['train-B','train-D','test-B','test-D']):
			values = [status[data][metric] for status in trails]
			if metric=='accuracy':
				values = [1-v for v in values]
			ax.plot(values, lw=0.8, color=cm.Set1(i), label=data)

		ax.minorticks_off()
		
		if metric=='accuracy':
#			ax.set_ylim(0,1)
			ax.yaxis.set_major_formatter(ticker.ScalarFormatter())
			ax.yaxis.set_major_formatter(plt.FormatStrFormatter('%.2f'))#y軸小数点以下3桁表示

		num_epoch = len(values)
		xticks = range(0,num_epoch+1,num_epoch//5)
		for b in xticks:
			ax.axvline(x=b, color='gray',lw=0.4,zorder=-3, linestyle='-', alpha=1.0)

		ax.legend(ncol=2, handletextpad=0.4,labelspacing=0.3,columnspacing=0.4)
		ax.set_xticks(xticks)
		ax.set_xticklabels(xticks)
		ax.set_title(metric)
		ax.set_xlabel('epoch')
		ax.set_yscale('log')
	
	os.makedirs(os.path.dirname(file_name),exist_ok=True)
	plt.savefig(file_name,dpi=120)
	plt.style.use('default');plt.clf();plt.cla();plt.close()


# -------------------------------------------------------------------
import os
import json
import pickle
import math
def main():
	report('start draw')	

	archive = 'MIRROR/ARCHIVEs/'
	if not os.path.exists(archive):	return

	for d in os.listdir(archive):
		if '.DS' in d: continue

		with open(f'{archive}/{d}/config.json', 'r') as fp:
			config = json.load(fp)
		lr	 = config['lr']
		data = config['dataset']
		seed = config['seed']

		with open(f'{archive}/{d}/trails.pkl','rb') as fp:
			trails = pickle.load(fp)

		output_file = f"VIEW/data={data}/lr={lr:4.3f},seed={seed}.png"
		draw(trails, output_file)
#		return

	report('finish')	
	print('-'*50)


# -------------------------------------------------------------------
if __name__=='__main__':
	main()


