#!/usr/bin/env python
# coding: utf-8

import sys


# -------------------------------------------------------------------
import datetime
def report(*args):
	print(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')+' '+' '.join(map(str,args)).replace('\n',''))


# -------------------------------------------------------------------
# -------------------------------------------------------------------
# -------------------------------------------------------------------
import os
import matplotlib.cm as cm
import matplotlib.pyplot as plt
from matplotlib import pyplot as plt
import numpy
def draw(buckets, file_name):
	print(f'draw:{file_name}')
	fig = plt.figure(figsize=(5,3.5))
	plt.subplots_adjust(left=0.17, right=0.99, top=0.92, bottom=0.15)


	lr = [0.01, 0.05, 0.1, 0.5]

	ax = fig.add_subplot(1,1,1)

	for i,key in enumerate(['B','D']):
		y = [buckets[key,v] for v in lr]
		ax.plot(lr,y,label=key, marker='+', color=cm.tab10(i), markersize=7, lw=1.0, zorder=12)
	legend = ax.legend(loc='upper left')
	legend.get_frame().set_alpha(1)
	legend.get_frame().set_zorder(1)
	ax.set_xscale('log')

	os.makedirs(os.path.dirname(file_name),exist_ok=True)
	plt.savefig(file_name,dpi=120)


# -------------------------------------------------------------------
import os, json, pickle
from collections import defaultdict
def load_results():
	buckets = {data:defaultdict(list) for data in ['mnist','cifar10']}

	archive = 'MIRROR/ARCHIVEs/'
	for d in os.listdir(archive):
		if '.DS' in d: continue

		file_name = f'{archive}/{d}/config.json'
		with open(file_name, 'r') as fp:
			config = json.load(fp)
		lr 		= config['lr']
		data 	= config['dataset']

		file_name = f'{archive}/{d}/trails.pkl'
		with open(file_name, 'rb') as fp:
			trails = pickle.load(fp)
		buckets[data]['B',lr].append(trails[-1]['test-B']['accuracy'])
		buckets[data]['D',lr].append(trails[-1]['test-D']['accuracy'])

	for data in buckets:
		for key in buckets[data]:
			buckets[data][key] = 1 - sum(buckets[data][key])/len(buckets[data][key])
		yield data,buckets[data]


# -------------------------------------------------------------------
def main():
	report('start draw')	

	for data,buckets in load_results():
		output_file = f"VIEW/sumup-{data}.png"
		draw(buckets, output_file)

	report('finish')
	print('-'*50)



# -------------------------------------------------------------------
if __name__=='__main__':
	main()


