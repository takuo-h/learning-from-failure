#!/usr/bin/env python
# coding: utf-8

import torch
torch.backends.cudnn.deterministic = True
torch.backends.cudnn.benchmark = False

# -------------------------------------------------------------------
import datetime
def report(*args):
	print(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')+' '+' '.join(map(str,args)).replace('\n',''))

# -------------------------------------------------------------------
import os, math
import torch.nn.functional as F
def train(datasets, models, config):
	def get_weight_b(logit_b, y):
		p = F.softmax(logit_b, dim=1)					# (b,K) # probability over all labels
		p = p.gather(dim=1, index=y.unsqueeze(dim=1))	# (b,)	# probability of true label
		weights = config.q * (p**config.q)				# (b)
		return weights.detach()

	# EMA(exponential moving average) of loss
	ema_b = torch.zeros(datasets.train.n).to(config.gpu)
	ema_d = torch.zeros(datasets.train.n).to(config.gpu)
	# maximum (EMA) loss over labels
	max_b = -math.inf * torch.ones(config.K).to(config.gpu)
	max_d = -math.inf * torch.ones(config.K).to(config.gpu)

	def get_weight_d(loss_b, loss_d, y, i):
		nonlocal max_b, max_d
		# update EMA loss
		ema_b[i] = config.alpha*ema_b[i] + (1-config.alpha)*loss_b
		ema_d[i] = config.alpha*ema_d[i] + (1-config.alpha)*loss_d
		# get latest loss
		loss_b = ema_b[i]
		loss_d = ema_d[i]
		# update maximum EMA loss
		batch_max_b = loss_b.unsqueeze(dim=1)			# (b) -> (b,1)
		batch_max_d = loss_d.unsqueeze(dim=1)			# (b) -> (b,1)
		batch_max_b = batch_max_b.repeat(1, config.K)	# -> (b,K)
		batch_max_d = batch_max_d.repeat(1, config.K)	# -> (b,K)
		mask = (F.one_hot(y,num_classes=config.K)==1)	# (b,K)
		batch_max_b[~mask] = -math.inf
		batch_max_d[~mask] = -math.inf
		batch_max_b,_ = batch_max_b.max(dim=0)			# -> (K)
		batch_max_d,_ = batch_max_d.max(dim=0)			# -> (K)
		max_b = torch.maximum(max_b, batch_max_b)
		max_d = torch.maximum(max_d, batch_max_d)
		# class-wise normalize
		loss_b /= max_b[y]
		loss_d /= max_d[y]
		weights = loss_b / (loss_b + loss_d + 1e-8)
		return weights.detach()

	optimizer_b = torch.optim.SGD(models.b.parameters(),lr=config.lr,momentum=config.momentum)
	optimizer_d = torch.optim.SGD(models.d.parameters(),lr=config.lr,momentum=config.momentum)
	def update(dataset, epoch):
		measure = {m:{'loss':0, 'accuracy':0} for m in 'BD'}
		models.b.train()
		models.d.train()
		for i,x,y in dataset:
			logit_b = models.b(x)
			logit_d = models.d(x)

			loss_b	= F.cross_entropy(logit_b, y, reduction='none')
			loss_d	= F.cross_entropy(logit_d, y, reduction='none')

			weight_d = get_weight_d(loss_b.detach(), loss_d.detach(), y, i)
			weight_b = get_weight_b(logit_b.detach(), y)

			loss	= (loss_b * weight_b).mean() + (loss_d*weight_d).mean()

			optimizer_b.zero_grad()
			optimizer_d.zero_grad()
			loss.backward()
			optimizer_b.step()
			optimizer_d.step()

			measure['B']['loss']		+= loss_b.sum().item()
			measure['D']['loss']		+= loss_d.sum().item()
			measure['B']['accuracy']	+= (logit_b.argmax(dim=1)==y).sum().item()
			measure['D']['accuracy']	+= (logit_d.argmax(dim=1)==y).sum().item()
		return {m:{k:v/dataset.n for k,v in measure[m].items()} for m in 'BD'}

	trails = []
	for epoch in range(config.num_epochs):
		status = {}
		status['train']	 = update(datasets.train, epoch)
		status['test-B'] = commons.eval_model( models.b, datasets.test)
		status['test-D'] = commons.eval_model( models.d, datasets.test)
		for mode in 'BD':	# split status just for print in following parts
			status[f'train-{mode}']	= status['train'][mode]
		del status['train']
#		status['ema_b'] = ema_b.cpu().detach().numpy()
#		status['ema_d'] = ema_b.cpu().detach().numpy()
		trails.append(status)
		
		report(f'epoch:{epoch}/{config.num_epochs}')
		for key in ['train-B','train-D','test-B','test-D']:
			message	 = [f'\t{key:8}']
			message	+= [f"loss:{status[key]['loss']: 15.7f}"]
			message	+= [f"accuracy:{status[key]['accuracy']: 9.7f}"]
			report(*message)
	return trails	

# -------------------------------------------------------------------
import argparse
from collections import namedtuple
def get_config():
	args = argparse.ArgumentParser(add_help=False)
	# general setting
	args.add_argument('--workroom',			default='workroom',	type=str)
	args.add_argument('--dataset',			default='mnist',	type=str,	choices=['mnist','cifar10'])
	args.add_argument('--seed',				default=7,			type=int)
	args.add_argument('--gpu',				default=0,			type=int)
	# for training
	args.add_argument('--batch_size',		default=64,			type=int)
	args.add_argument('--lr',				default=0.05,		type=float)
	args.add_argument('--momentum',			default=0.0,		type=float)
	args.add_argument('--num_epochs',		default=3,			type=int)
	# exponential moving average
	args.add_argument('--alpha',			default=0.7,		type=float)	# for exponential moving average
	args.add_argument('--q',				default=0.7,		type=float)	# for generalized cross entropy
	args.add_argument('--K',				default=10,			type=int)	# number-of-class
	# for evaluation
	args.add_argument('--test_batch_size',	default=10000,		type=int)
	args = vars(args.parse_args())
	return namedtuple('_',args.keys())(**args)	# set all settings immutable

# -------------------------------------------------------------------
import commons, copy
import json, pickle,os
def main():
	print('-'*50)
	report('start')

	config      = get_config()
	datasets	= commons.get_dataset(config)
	model 		= commons.get_model(config)
	models 		= namedtuple('_','b d')(b=model, d=copy.deepcopy(model))
	trails 		= train(datasets, models, config)

	os.makedirs(config.workroom, exist_ok=True)
	with open(f'{config.workroom}/config.json', 'w') as fp:
		json.dump(config._asdict(), fp, indent=2)
	with open(f'{config.workroom}/trails.pkl', 'wb') as fp:
		pickle.dump(trails, fp)

	report('finished')
	print('-'*50)
	
if __name__ == '__main__':
	main()
