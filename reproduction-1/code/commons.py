#!/usr/bin/env python
# coding: utf-8

import torch
torch.backends.cudnn.deterministic = True
torch.backends.cudnn.benchmark = False

# -------------------------------------------------------------------
import datetime
def report(*args):
	print(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')+' '+' '.join(map(str,args)).replace('\n',''))

# -------------------------------------------------------------------
import torch.nn.functional as F
def compute_gradient(model, dataset):
	model.eval()
	model.zero_grad()
	for x,y in dataset:
		logit = model(x)
		loss  = F.cross_entropy(logit, y, reduction='sum')
		loss.backward()
	# chone parameters and scale them by 1/n
	query = [p.grad.data.clone()/dataset.n for p in model.parameters()]
	query = [q.requires_grad_(False) for q in query]
	return query

# -------------------------------------------------------------------
import torch.nn.functional as F
def eval_model(model, dataset):
	measure = {'loss':0, 'accuracy':0}
	model.eval()
	with torch.inference_mode():
		Loss, accuracy = 0,0
		for x,y in dataset:
			logit	= model(x)
			loss	= F.cross_entropy(logit, y, reduction='sum')
			measure['loss'] 	+= loss.item()
			measure['accuracy']	+= (logit.argmax(dim=1)==y).sum().item()
	return {k:v/dataset.n for k,v in measure.items()}


# -------------------------------------------------------------------
#from . import data_zoo
import data_zoo
def get_dataset(config):
	args = {}
	args['train_batch_size'] = config.batch_size
	args['test_batch_size']  = config.test_batch_size
	args['seed'] 			 = config.seed
	args['gpu']				 = config.gpu
	if config.dataset=='mnist':
		return data_zoo.MNIST(**args)
	if config.dataset=='cifar10':
		return data_zoo.CIFAR10(**args)

#from . import model_zoo
import model_zoo
def get_model(config):
	torch.manual_seed(config.seed)
	if config.dataset=='mnist':
		model = model_zoo.MnistNet()
	if config.dataset=='cifar10':
		model = model_zoo.CifarNet()
	model = model.cuda(config.gpu)
	return model




# -------------------------------------------------------------------

