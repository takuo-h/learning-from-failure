#!/usr/bin/env python
# coding: utf-8

# -------------------------------------------------------------------
# common procedure
import torch
import torchvision
def _get_simplified_dataset(data_name, split, is_cuda):
	transforms = torchvision.transforms.ToTensor()	# just convert into pytorch tensor
	# options are argument given to dataset
	options	= {'root':f'DATASETs/{data_name}','download':True,'transform':transforms}
	if data_name=='SVHN':	options |= {'split':split}				# SVHN has three splits, train, valid, and test
	else:					options |= {'train':(split=='train')}
	# select dataclass
	if data_name=='MNIST':		dataclass = torchvision.datasets.MNIST
	if data_name=='CIFAR10':	dataclass = torchvision.datasets.CIFAR10
	if data_name=='CIFAR100':	dataclass = torchvision.datasets.CIFAR100
	if data_name=='SVHN':		dataclass = torchvision.datasets.SVHN
	dataset = dataclass(**options)
	# extract x and y to simplify dataset
	x = torch.stack( [v[0] for v in dataset])
	y = torch.tensor([v[1] for v in dataset])
	if is_cuda:
		x = x.cuda()
		y = y.cuda()
	return x,y

class _SimplifiedGenerator(object):
	def __init__(self, x, y, batch_size, is_shuffle, augmentations=None):
		self.x, self.y	= x,y
		self.batch_size = batch_size
		self.is_shuffle = is_shuffle
		self.augmentations = augmentations
		self.n = len(x)
	def __iter__(self):
		if self.is_shuffle:
			index = torch.randperm(self.n)
			self.x = self.x[index]
			self.y = self.y[index]

		x = self.x
		if self.augmentations is not None:
			x = self.augmentations(x).detach()

		for i in range(0, self.n, self.batch_size):
			bx =	  x[i:i+self.batch_size]
			by = self.y[i:i+self.batch_size]
			yield bx,by

# -------------------------------------------------------------------
from collections import namedtuple
def _generic_process(data_name, is_cuda, train_batch_size, test_batch_size, augmentations, num_classes):
	from . import augmentation_zoo as aug_zoo
	augmentations = aug_zoo.get_transforms(augmentations)	# this argumentation aims to process data simultaneously
	train = _get_simplified_dataset(data_name=data_name, split='train', is_cuda=is_cuda)
	test  = _get_simplified_dataset(data_name=data_name, split='test',  is_cuda=is_cuda)
	train = _SimplifiedGenerator(*train, train_batch_size, is_shuffle=True, augmentations=augmentations)
	test  = _SimplifiedGenerator(*test,  test_batch_size,  is_shuffle=False)
	return namedtuple('_','train test num_classes')(train=train, test=test, num_classes=num_classes)	

def MNIST(**kwargs):
	kwargs['augmentations'] = []
	return _generic_process(data_name='MNIST', 	  num_classes=10, 	**kwargs)

def CIFAR10(**kwargs):
	kwargs['augmentations'] = ['Crop','Flip']
	return _generic_process(data_name='CIFAR10',  num_classes=10,	**kwargs)

def SVHN(**kwargs):
	kwargs['augmentations'] = ['Crop']
	return _generic_process(data_name='SVHN',		num_classes=10,		**kwargs)

def CIFAR100(**kwargs):
	kwargs['augmentations'] = ['Crop','Flip']
	return _generic_process(data_name='CIFAR100', num_classes=100,	**kwargs)

# -------------------------------------------------------------------




