#!/usr/bin/env python
# coding: utf-8

import datetime
def report(*args):
	print(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')+' '+' '.join(map(str,args)).replace('\n',''))

# -------------------------------------------------------------------
import argparse
import datetime
def get_common_args():
	args = argparse.ArgumentParser(add_help=False)
	# general
	args.add_argument('--workroom',			default='workroom',	type=str)
	args.add_argument('--seed',				default=0,			type=lambda x: int(x) if x.isdecimal() else int(time.time()) )
	args.add_argument('--gpu',				default=-1,			type=lambda x: int(x) if (torch.cuda.is_available() and 0<=int(x)<torch.cuda.device_count()) else -1)

	# dataset and model
	args.add_argument('--model',			default='SmallCNN',		type=str, 	choices=['SmallCNN', 'MadryLeNet', 'PResNet18','WRN-32-10','WRN-34-10'])
	args.add_argument('--dataset',			default='MNIST',		type=str,	choices=['MNIST','CIFAR10','CIFAR100','SVHN'])
	
	# optimization
	args.add_argument('--epoch_length',		default=50,			type=int)
	args.add_argument('--batch_size',		default=2**10,		type=int)
	args.add_argument('--eval_batch_size',	default=2**9,		type=int)
	args.add_argument('--lr',				default=0.1,		type=float)
	return args

# -------------------------------------------------------------------
import time
import random
import numpy
import torch
def _set_random_seed(config):
	random.seed(config.seed)
	numpy.random.seed(config.seed)
	torch.manual_seed(config.seed)	
	report(f'set random-seed:{config.seed}')

import torch
def _set_device(config):
	if config.gpu<0:
		report('use CPU in this trial')
	else:
		report(f'use GPU; gpu:{config.gpu}')
		torch.cuda.set_device(config.gpu)
	
# -------------------------------------------------------------------
def _get_data(config):
	report(f'get data:{config.dataset}')
	args = {}
	args['train_batch_size'] = config.batch_size
	args['test_batch_size']  = config.eval_batch_size
	args['is_cuda']			 = (0<=config.gpu)
	from . import data_zoo
	if config.dataset=='MNIST':		return data_zoo.MNIST(**args)
	if config.dataset=='CIFAR10':	return data_zoo.CIFAR10(**args)
	if config.dataset=='CIFAR100':	return data_zoo.CIFAR100(**args)
	if config.dataset=='SVHN':		return data_zoo.SVHN(**args)
	assert False, (f'no such dataset:{config.dataset}')

def _get_model(config, num_classes):
	report(f'get model:{config.model}')
	from . import model_zoo
	if config.model=='SmallCNN':	model = model_zoo.SmallCNN(num_classes=num_classes)
	if config.model=='MadryLeNet':	model = model_zoo.MadryLeNet(num_classes=num_classes)
	if config.model=='PResNet18':	model = model_zoo.PreActResNet(num_classes=num_classes)
	if config.model=='WRN-32-10':	model = model_zoo.WideResNet(depth=32,widen_factor=10,num_classes=num_classes)
	if config.model=='WRN-34-10':	model = model_zoo.WideResNet(depth=34,widen_factor=10,num_classes=num_classes)
	if 0<=config.gpu:				model = model.cuda()
	return model

def setup(config):
	_set_random_seed(config)
	_set_device(config)
	data  = _get_data(config)
	model = _get_model(config, data.num_classes)
	return data, model

# -------------------------------------------------------------------
