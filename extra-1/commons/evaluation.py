#!/usr/bin/env python
# coding: utf-8

# -------------------------------------------------------------------
import torch
import torch.nn.functional as F
def clean(data, model):
	model.eval()
	measure = {'loss':[],'accuracy':[]}
	with torch.inference_mode():
		for x,y in data:
			logit	= model(x)
			loss	= F.cross_entropy(logit, y, reduction='sum')
			measure['loss'] 	+= loss.detach().cpu()
			measure['accuracy'] += (logit.argmax(dim=1)==y).sum()
	return {k:(v/data.n).item() for k,v in measure.items()}
	
# -------------------------------------------------------------------


