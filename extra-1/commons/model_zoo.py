#!/usr/bin/env python
# coding: utf-8

# -------------------------------------------------------------------
import copy
import torch
class SaveLoadBase(torch.nn.Module):
	def get_param(self):
		state = copy.deepcopy(self.state_dict())
		for k in state:
			state[k] = state[k].cpu().detach().numpy()
		return state
	def set_param(self, param, is_cuda=False):
		for k in param:
			param[k] = torch.tensor(param[k],dtype=torch.float)
		if is_cuda:
			param = {k:v.cuda() for k,v in param.items()}
		self.load_state_dict(param)	




# -------------------------------------------------------------------
# just for debug
import torch
class SmallCNN(SaveLoadBase):
	def __init__(self, num_classes=10):
		super(SmallCNN, self).__init__()
		self.conv1 = torch.nn.Conv2d(1, 20, 5, stride=1, padding=0)
		self.conv2 = torch.nn.Conv2d(20,20, 5, stride=1, padding=0)
		self.L = torch.nn.Linear(4*4*20, num_classes)

	def forward(self, x):								# := (b, 1,28,28)	# (batch, channel, width, height)
		x = torch.nn.functional.relu(self.conv1(x))		# -> (b,20,24,24)
		x = torch.nn.functional.max_pool2d(x, 2, 2)		# -> (b,20,12,12)
		x = torch.nn.functional.relu(self.conv2(x))		# -> (b,20, 8, 8)
		x = torch.nn.functional.max_pool2d(x, 2, 2)		# -> (b,20, 4, 4)
		x = x.flatten(1,-1)								# -> (b,20*4*4)
		return self.L(x)								# -> (b,10)






# -------------------------------------------------------------------
import torch
class MadryLeNet(SaveLoadBase):
	def __init__(self, num_classes=10):
		super(MadryLeNet, self).__init__()
		self.layer0	= torch.nn.Conv2d(  1, 32,  3, stride=1, padding=1)
		self.bnorm0	= torch.nn.BatchNorm2d(32)
		self.layer1	= torch.nn.Conv2d( 32, 64,  3, stride=1, padding=1)
		self.bnorm1	= torch.nn.BatchNorm2d(64)
		self.layer2	= torch.nn.Conv2d( 64,128,  3, stride=1, padding=1)
		self.bnorm2	= torch.nn.BatchNorm2d(128)
		self.layer3	= torch.nn.Conv2d(128,256,  3, stride=1, padding=1)
		self.bnorm3	= torch.nn.BatchNorm2d(256)
		self.layer4	= torch.nn.Linear(7*7*256, 1024)
		self.bnorm4	= torch.nn.BatchNorm1d(1024)
		self.layer5	= torch.nn.Linear(   1024, num_classes)

	def forward(self, x):							# := (b,  1,28,28)	# (batch, channel, width, height)
		x = torch.relu(self.bnorm0(self.layer0(x)))	# -> (b, 32,28,28)
		x = torch.relu(self.bnorm1(self.layer1(x)))	# -> (b, 64,28,28)
		x = torch.nn.functional.max_pool2d(x, 2, 2)	# -> (b, 64,14,14)

		x = torch.relu(self.bnorm2(self.layer2(x)))	# -> (b,128,14,14)
		x = torch.relu(self.bnorm3(self.layer3(x)))	# -> (b,256,14,14)
		x = torch.nn.functional.max_pool2d(x, 2, 2)	# -> (b,256, 7, 7)

		x = x.flatten(1,-1)							# -> (b,256*7*7)
		x = torch.relu(self.bnorm4(self.layer4(x)))	# -> (b,1024)
		return self.layer5(x)						# -> (b,10)





# -------------------------------------------------------------------
import torch
import torch.nn as nn
import torch.nn.functional as F
class PreActBlock(nn.Module):
	'''Pre-activation version of the BasicBlock.'''
	expansion = 1
	def __init__(self, in_planes, planes, stride=1):
		super(PreActBlock, self).__init__()
		self.bn1	= nn.BatchNorm2d(in_planes)
		self.conv1	= nn.Conv2d(in_planes, planes, kernel_size=3, stride=stride, padding=1, bias=False)
		self.bn2	= nn.BatchNorm2d(planes)
		self.conv2	= nn.Conv2d(planes, planes, kernel_size=3, stride=1, padding=1, bias=False)
		if stride != 1 or in_planes != self.expansion*planes:
			self.shortcut = nn.Conv2d(in_planes, self.expansion*planes, kernel_size=1, stride=stride, bias=False)
	def forward(self, x):
		shortcut = self.shortcut(x) if hasattr(self, 'shortcut') else x
		out = self.conv1(F.relu(self.bn1(x)))
		out = self.conv2(F.relu(self.bn2(out)))
		out = out+shortcut
		return out

class PreActResNet(SaveLoadBase):
	def __init__(self, num_blocks=[2,2,2,2], num_classes=10):
		super(PreActResNet, self).__init__()
		block = PreActBlock
		self.in_planes = 64
		self.conv1	= nn.Conv2d(3, 64, kernel_size=3, stride=1, padding=1, bias=False)
		self.layer1	= self._make_layer(block,  64, num_blocks[0], stride=1)
		self.layer2	= self._make_layer(block, 128, num_blocks[1], stride=2)
		self.layer3	= self._make_layer(block, 256, num_blocks[2], stride=2)
		self.layer4	= self._make_layer(block, 512, num_blocks[3], stride=2)
		self.bn 	= nn.BatchNorm2d(512 * block.expansion)
		self.linear	= nn.Linear(512 * block.expansion, num_classes)

	def _make_layer(self, block, planes, num_blocks, stride):
		strides = [stride] + [1]*(num_blocks-1)
		layers = []
		for stride in strides:
			layers.append(block(self.in_planes, planes, stride))
			self.in_planes = planes * block.expansion
		return nn.Sequential(*layers)

	def forward(self, x):
		out = self.conv1(x)
		out = self.layer1(out)
		out = self.layer2(out)
		out = self.layer3(out)
		out = self.layer4(out)
		out = F.relu(self.bn(out))
		out = F.avg_pool2d(out, 4)
		out = out.view(out.size(0), -1)
		out = self.linear(out)
		return out



# -------------------------------------------------------------------
import torch
class BasicBlock(torch.nn.Module):
	def __init__(self, ci, co, stride=1):	# ci: input channel size, co: outout channel size
		super(BasicBlock, self).__init__()
		self.bn1	= torch.nn.BatchNorm2d(ci)
		self.conv1	= torch.nn.Conv2d(ci, co, kernel_size=3, padding=1, bias=False, stride=stride)
		self.bn2	= torch.nn.BatchNorm2d(co)
		self.conv2	= torch.nn.Conv2d(co, co, kernel_size=3, padding=1, bias=False, stride=1)
		self.need_shortcut = (ci != co)
		if self.need_shortcut:
			self.shortcut = torch.nn.Conv2d(ci, co, kernel_size=1, padding=0, bias=False, stride=stride)
	def forward(self, x):											# (b,d,w,h) : c in [ci, co]
		if self.need_shortcut:
			x = torch.nn.functional.relu(self.bn1(x))				# -> (b,ci,w,h)
			z = torch.nn.functional.relu(self.bn2(self.conv1(x)))	# -> (b,co,w,h)
			x = self.shortcut(x)									# -> (b,co,w,h)
		else:
			z = torch.nn.functional.relu(self.bn1(x))				# -> (b,co,w,h)
			z = torch.nn.functional.relu(self.bn2(self.conv1(z)))	# -> (b,co,w,h)
		return x+self.conv2(z)										# -> (n,co,w,h)

class NetworkBlock(torch.nn.Module):
	def __init__(self, num_of_layers, ci, co, stride):	# ci: input channel size, co: outout channel size
		super(NetworkBlock, self).__init__()
		self.in_block = BasicBlock(ci=ci,co=co,stride=stride)
		layers = [ BasicBlock(ci=co,co=co) for _ in range(1,num_of_layers) ]
		self.middle_blocks = torch.nn.Sequential(*layers)
	def forward(self, x):				# -> (b, ci, w, h)	
		return self.in_block(x)			# -> (b, co, w//stride, h//stride)
		return self.middle_blocks(x)	# -> (b, co, w//stride, h//stride)
	
import torch
class WideResNet(SaveLoadBase):
	def __init__(self, depth, widen_factor, num_classes=10):
		super(WideResNet, self).__init__()
		assert ((depth - 4) % 6 == 0)
		channels	= [16, 16*widen_factor, 32*widen_factor, 64*widen_factor]
		blocklayer	= (depth - 4) // 6
		self.conv0  = torch.nn.Conv2d(                   3, channels[0], kernel_size=3, stride=1, padding=1, bias=False)
		self.block1 = NetworkBlock(blocklayer, channels[0], channels[1], stride=1)
		self.block2 = NetworkBlock(blocklayer, channels[1], channels[2], stride=2)
		self.block3 = NetworkBlock(blocklayer, channels[2], channels[3], stride=2)
		self.bn		= torch.nn.BatchNorm2d(channels[3])
		self.fc		= torch.nn.Linear(channels[3], num_classes)

	def forward(self, x):							# -> (b,  3, 32,32)
		x = self.conv0(x)							# -> (b, 16, 32,32)
		x = self.block1(x)							# -> (b, d1, 32,32) : d1 = 16 * widen_factor
		x = self.block2(x)							# -> (b, d2, 16,16) : d2 = 32 * widen_factor
		x = self.block3(x)							# -> (b, d3,  8, 8) : d2 = 64 * widen_factor
		x = torch.nn.functional.relu(self.bn(x))	# -> (b, d3,  8, 8)
		x = torch.nn.functional.avg_pool2d(x, 8)	# -> (b, d3,  1, 1)
		x = x.squeeze()								# -> (b, d3)
		return self.fc(x)							# -> (b, num_classes)