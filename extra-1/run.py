 #!/usr/bin/env python
# coding: utf-8

# ------------------------------------------------------
from os import path
import query_queue_and_parallel as qqp
import numpy as np
def main():
	query = qqp.Query()

	query['scripts']		= [path.join(path.dirname(__file__),'main.py')]
	query['dataset']		= ['CIFAR10']
	query['model']			= ['PResNet18']
	query['seed']			= [-1 for _ in range(1)]
	query['lr']				= np.logspace(-3,0,8).tolist()
	# [0.001, 0.00268, 0.00720, 0.0193, 0.0518, 0.1389, 0.373, 1.0]
	query.ready()
	query.run(GPU_index=range(8))	

if __name__ == '__main__':
	main()



# ----------------------
