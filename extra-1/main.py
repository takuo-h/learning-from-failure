#!/usr/bin/env python
# coding: utf-8

import torch
torch.set_num_threads(3)
torch.set_num_interop_threads(3)
torch.backends.cudnn.benchmark = True

# -------------------------------------------------------------------
import datetime
def report(*args):
	print(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')+' '+' '.join(map(str,args)).replace('\n',''))

# -------------------------------------------------------------------
import torch, math, copy
import torch.nn.functional as F
from commons import evaluation
def optimize(dataset, model, config):
	report('start optimization')

	modelB = copy.deepcopy(model)
	modelD = model

	optimizerB = torch.optim.SGD(modelB.parameters(), lr=config.lr, momentum=0.9, weight_decay=5e-4)
	optimizerD = torch.optim.SGD(modelD.parameters(), lr=config.lr, momentum=0.9, weight_decay=5e-4)

	def GCE(logit, y, q=0.75):
		p = F.softmax(logits, dim=1)
		Yg = torch.gather(p, 1, torch.unsqueeze(y, 1))
		loss_weight = (Yg.squeeze().detach()**q)*q
		return F.cross_entropy(logits, y, reduction='none') * loss_weight

	def update(data):
		measure = {f'{metric}{target}':0 for target in ['B-G','B-P','D'] for metric in ['loss','accuracy']}
		model.train()
		for x,y in data:
			# update biased model
			logit = modelB(x)
			loss  = GCEloss(logit, y)

			measure['lossB-G']		+= loss * len(y)
			measure['accuracyB-G']	+= (logit.argmax(dim=1)==y).sum()

			optimizerB.zero_grad()
			loss.backward()
			optimizerB.step()

			# update de-bias model
			logit = modelB(x)
			lossB = F.cross_entropy(logit, y, reduction='none')
			lossB = lossB.detach()

			measure['lossB-P']		+= lossB.sum()
			measure['accuracyB-P']	+= (logit.argmax(dim=1)==y).sum()

			logit = modelD(x)
			lossD = F.cross_entropy(logit, y, reduction='none')

			measure['lossD'] 	 += lossD.sum().item()
			measure['accuracyD'] += (logit.argmax(dim=1)==y).sum()

			loss = 1/(1/lossD + 1/lossB)
			loss = loss.sum()
			optimizerD.zero_grad()
			loss.backward()
			optimizerD.step()
		return {k:(v/data.n).item() for k,v in measure.items()}
	
	trails = []
	for epoch in range(config.epoch_length):
		status = {}
		status['train'] = update(dataset.train)
		status['test']	= evaluation.clean( dataset.test, model)
		trails.append(status)
		
		report(f'epoch:{epoch}/{config.epoch_length}')
		key = 'train'
		for target in ['B-G','B-P','D']:
			loss, accuracy = status[key][f'loss{target}'], status[key][f'accuracy{target}']
			message	 = [f'\t{key:6}{target:>3}']
			message	+= [f"loss:{loss: 15.7f}"]
			message	+= [f"accuracy:{accuracy: 9.7f}"]
			report(*message)
		key = 'test'
		loss, accuracy = status[key][f'loss'], status[key][f'accuracy']
		message	 = [f'\t{key:6}']
		message	+= [f"loss:{loss: 15.7f}"]
		message	+= [f"accuracy:{accuracy: 9.7f}"]
		report(*message)


		if not math.isfinite(status['train']['loss']):
			print('encounter non-finite loss in train')
			return trails, None

	return trails


# ------------------------------------------------------
import argparse
from collections import namedtuple
def get_config():
	from commons.preprocess import get_common_args
	args = argparse.ArgumentParser(parents=[get_common_args()])
	args.add_argument('--q',			default=0.75,			type=float)
	args = vars(args.parse_args())
	return namedtuple('_',args.keys())(**args)

from commons import preprocess
import os, json, pickle
def main():
	report('open experiment')

	config = get_config()
	dataset, model = preprocess.setup(config)
	trails = optimize(dataset, model, config)

	os.makedirs(config.workroom, exist_ok=True)
	with open(f'{config.workroom}/config.json', 'w') as fp:
		json.dump(config._asdict(), fp, indent=2)
	with open(f'{config.workroom}/trails.pkl', 'wb') as fp:
		pickle.dump(trails, fp)

	report('close experiment')

# -------------------------------------------------------------------	
if __name__=='__main__':
	main()

# -------------------------------------------------------------------

